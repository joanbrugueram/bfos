#include "x86asm.h"
#include "../mystdlib.h"

/// Kinds of X86 labels.
typedef enum X86EmitterLabelKind X86EmitterLabelKind;

enum X86EmitterLabelKind
{
	/// Label to an absolute memory position.
	X86EmitterLabelKind_Absolute,
	/// Label to a relative memory position (inside the code block).
	X86EmitterLabelKind_Relative
};

/// A label (address reference) in the X86 emitter.
typedef struct X86EmitterLabel X86EmitterLabel;

struct X86EmitterLabel
{
	/// Kind of label.
	X86EmitterLabelKind kind;

	union
	{
		/// Absolute memory address; for absolute labels
		void *dst;
		/// Offset (from beginning of block); for relative labels
		size_t offset;
	};
};

/// Structure to store jump/call addresses to be fixed later.
typedef struct X86EmitterFixup X86EmitterFixup;

struct X86EmitterFixup
{
	/// Label associated with the jump/call.
	X86EmitterLabelId labelId;
	/// Index in the code array of the first byte of the address to be patched.
	/// For example in:

	/// 0x00000000: 90          | NOP
	/// 0x00000001: E8 XXXXXXXX | CALL XXXXXXXX
	/// 0x00000006: 90          | NOP
	/// This would be 0x00000002.
	size_t patchPoint;
	/// Index in the code array to the byte where the address in relative to.
	/// In x86, addresses are typically written relative to the end of the instruction.
	/// In our previous example, this would be 0x00000006.
	size_t relaPoint;
};

/// Internal X86 emitter main structure.
struct X86Emitter
{
	/// Array of generated labels.
	X86EmitterLabel *labels;
	/// Capacity (in items) of the labels array.
	size_t labelsCapacity;
	/// Index of the next label to assign.
	size_t labelsPos;

	/// Array of assigned code fixups.
	X86EmitterFixup *fixups;
	/// Capacity (in items) of the fixups array.
	size_t fixupsCapacity;
	/// Index of the next fixup to assign.
	size_t fixupsPos;

	/// Array containing the machine code being generated.
	uint8_t *code;
	/// Capacity (in items) of the code array.
	size_t codeCapacity;
	/// Index of the next byte of code to be filled.
	size_t codePos;
};

X86Emitter *X86Emitter_Create()
{
	X86Emitter *emit = malloc(sizeof(X86Emitter));
	if (emit == NULL)
		return NULL;

	emit->labels = NULL;
	emit->labelsCapacity = 0;
	emit->labelsPos = 0;

	emit->fixups = NULL;
	emit->fixupsCapacity = 0;
	emit->fixupsPos = 0;

	emit->code = NULL;
	emit->codeCapacity = 0;
	emit->codePos = 0;

	return emit;
}

void X86Emitter_Destroy(X86Emitter *emit)
{
	if (emit != NULL)
	{
		free(emit->code);
		free(emit->labels);
		free(emit->fixups);
	}
	free(emit);
}

X86EmitterLabelId X86Emitter_CreateLabel(X86Emitter *emit)
{
	if (emit->labelsPos == emit->labelsCapacity)
	{
		emit->labelsCapacity += 256;
		emit->labels = realloc(emit->labels, sizeof(X86EmitterLabel) * emit->labelsCapacity);
		// TODO check error
	}

	return emit->labelsPos++;
}

void X86Emitter_SetLabelTo(X86Emitter *emit, X86EmitterLabelId labelId, void *dst)
{
	emit->labels[labelId].kind = X86EmitterLabelKind_Absolute;
	emit->labels[labelId].dst = dst;
}

void X86Emitter_SetLabel(X86Emitter *emit, X86EmitterLabelId labelId)
{
	emit->labels[labelId].kind = X86EmitterLabelKind_Relative;
	emit->labels[labelId].offset = emit->codePos;
}

static void X86Emitter_AddFixup(X86Emitter *emit, X86EmitterLabelId labelId,
                                size_t patchPoint, size_t relaPoint)
{
	if (emit->fixupsPos == emit->fixupsCapacity)
	{
		emit->fixupsCapacity += 256;
		emit->fixups = realloc(emit->fixups, sizeof(X86EmitterFixup) * emit->fixupsCapacity);
		// TODO check error
	}

	X86EmitterFixup *fixup = &emit->fixups[emit->fixupsPos++];
	fixup->labelId = labelId;
	fixup->patchPoint = patchPoint;
	fixup->relaPoint = relaPoint;
}

/// Adds a fixed 8-bit value to the machine code.
static void X86Emitter_EmitByte(X86Emitter *emit, uint8_t byte)
{
	if (emit->codePos == emit->codeCapacity)
	{
		emit->codeCapacity += 8192;
		emit->code = realloc(emit->code, sizeof(uint8_t) * emit->codeCapacity);
		// TODO check error
	}

	emit->code[emit->codePos++] = byte;
}

/// Adds a fixed 32-bit value to the machine code.
static void X86Emitter_EmitDWord(X86Emitter *emit, uint32_t dword)
{
	X86Emitter_EmitByte(emit, (dword >> 0) & 0xFF);
	X86Emitter_EmitByte(emit, (dword >> 8) & 0xFF);
	X86Emitter_EmitByte(emit, (dword >> 16) & 0xFF);
	X86Emitter_EmitByte(emit, (dword >> 24) & 0xFF);
}

void X86Emitter_EmitPushEax(X86Emitter *emit)
{
	X86Emitter_EmitByte(emit, 0x50);
}

void X86Emitter_EmitPopEax(X86Emitter *emit)
{
	X86Emitter_EmitByte(emit, 0x58);
}

void X86Emitter_EmitIncEax(X86Emitter *emit)
{
	X86Emitter_EmitByte(emit, 0x40);
}

void X86Emitter_EmitDecEax(X86Emitter *emit)
{
	X86Emitter_EmitByte(emit, 0x48);
}

void X86Emitter_EmitMovEaxImm32(X86Emitter *emit, uint32_t value)
{
	X86Emitter_EmitByte(emit, 0xB8);
	X86Emitter_EmitDWord(emit, value);
}

void X86Emitter_EmitAddEaxImm8(X86Emitter *emit, int8_t value)
{
	X86Emitter_EmitByte(emit, 0x83);
	X86Emitter_EmitByte(emit, 0xC0);
	X86Emitter_EmitByte(emit, value);
}

void X86Emitter_EmitAddEaxImm32(X86Emitter *emit, uint32_t value)
{
	X86Emitter_EmitByte(emit, 0x05);
	X86Emitter_EmitDWord(emit, value);
}

void X86Emitter_EmitCmpEaxImm32(X86Emitter *emit, uint32_t value)
{
	X86Emitter_EmitByte(emit, 0x3D);
	X86Emitter_EmitDWord(emit, value);
}

void X86Emitter_EmitMovBytePtrDsEaxImm8(X86Emitter *emit, uint8_t value)
{
	X86Emitter_EmitByte(emit, 0xC6);
	X86Emitter_EmitByte(emit, 0x00);
	X86Emitter_EmitByte(emit, value);
}

void X86Emitter_EmitIncBytePtrDsEax(X86Emitter *emit)
{
	X86Emitter_EmitByte(emit, 0xFE);
	X86Emitter_EmitByte(emit, 0x00);
}

void X86Emitter_EmitDecBytePtrDsEax(X86Emitter *emit)
{
	X86Emitter_EmitByte(emit, 0xFE);
	X86Emitter_EmitByte(emit, 0x08);
}

void X86Emitter_EmitAddBytePtrDsEaxImm8(X86Emitter *emit, uint8_t value)
{
	X86Emitter_EmitByte(emit, 0x80);
	X86Emitter_EmitByte(emit, 0x00);
	X86Emitter_EmitByte(emit, value);
}

void X86Emitter_EmitCmpBytePtrDsEaxImm8(X86Emitter *emit, uint8_t value)
{
	X86Emitter_EmitByte(emit, 0x80);
	X86Emitter_EmitByte(emit, 0x38);
	X86Emitter_EmitByte(emit, value);
}

void X86Emitter_EmitMovBytePtrDsEaxCl(X86Emitter *emit)
{
	X86Emitter_EmitByte(emit, 0x88);
	X86Emitter_EmitByte(emit, 0x08);
}

void X86Emitter_EmitMovzxEcxBytePtrDsEax(X86Emitter *emit)
{
	X86Emitter_EmitByte(emit, 0x0F);
	X86Emitter_EmitByte(emit, 0xB6);
	X86Emitter_EmitByte(emit, 0x08);
}

void X86Emitter_EmitJe(X86Emitter *emit, X86EmitterLabelId labelId)
{
	X86Emitter_AddFixup(emit, labelId, emit->codePos + 2, emit->codePos + 6);

	X86Emitter_EmitByte(emit, 0x0F);
	X86Emitter_EmitByte(emit, 0x84);
	X86Emitter_EmitDWord(emit, 0x1337B33F /* TO BE FILLED */);
}

void X86Emitter_EmitJne(X86Emitter *emit, X86EmitterLabelId labelId)
{
	X86Emitter_AddFixup(emit, labelId, emit->codePos + 2, emit->codePos + 6);

	X86Emitter_EmitByte(emit, 0x0F);
	X86Emitter_EmitByte(emit, 0x85);
	X86Emitter_EmitDWord(emit, 0x1337B33F /* TO BE FILLED */);
}

void X86Emitter_EmitCall(X86Emitter *emit, X86EmitterLabelId labelId)
{
	X86Emitter_AddFixup(emit, labelId, emit->codePos + 1, emit->codePos + 5);

	X86Emitter_EmitByte(emit, 0xE8);
	X86Emitter_EmitDWord(emit, 0x1337B33F /* TO BE FILLED */);
}

void X86Emitter_EmitRet(X86Emitter *emit)
{
	X86Emitter_EmitByte(emit, 0xC3);
}

void X86Emitter_EmitInt3(X86Emitter *emit)
{
	X86Emitter_EmitByte(emit, 0xCC);
}

void X86Emitter_EmitMovEcxEax(X86Emitter *emit)
{
	X86Emitter_EmitByte(emit, 0x8B);
	X86Emitter_EmitByte(emit, 0xC8);
}

void *X86Emitter_Link(X86Emitter *emit)
{
	// Apply fixups
	size_t oldCodePos = emit->codePos;

	for (size_t i = 0; i < emit->fixupsPos; i++)
	{
		const X86EmitterFixup *fixup = &emit->fixups[i];
		const X86EmitterLabel *label = &emit->labels[fixup->labelId];

		// Figure out the definitive destination of the pointer,
		// now that all labels *SHOULD* be correctly set and the code definitely located.
		void *finalDst;
		if (label->kind == X86EmitterLabelKind_Absolute)
			finalDst = label->dst;
		else
			finalDst = &emit->code[label->offset];

		// Figure out the relativity point, now that the code should be definitely located.
		void *relPtr = &emit->code[fixup->relaPoint];

		// Write the (relative) offset
		emit->codePos = fixup->patchPoint;
		X86Emitter_EmitDWord(emit, (uint32_t)finalDst - (uint32_t)relPtr);
	}

	emit->codePos = oldCodePos;

	return emit->code;
}
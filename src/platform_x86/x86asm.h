#ifndef BFOS_X86ASM_H
#define BFOS_X86ASM_H

#include <stddef.h>
#include <stdint.h>

typedef size_t X86EmitterLabelId;
typedef struct X86Emitter X86Emitter;

/// Create a new X86Emitter.
X86Emitter *X86Emitter_Create();
/// Destroy the given X86Emitter.
void X86Emitter_Destroy(X86Emitter *emit);

/// Create a label to an unspecified position (it will be destroyed automatically).
X86EmitterLabelId X86Emitter_CreateLabel(X86Emitter *emit);
/// Set the specified label to the specified absolute code position.
void X86Emitter_SetLabelTo(X86Emitter *emit, X86EmitterLabelId labelId, void *dst);
/// Set the specified label to the current code position.
void X86Emitter_SetLabel(X86Emitter *emit, X86EmitterLabelId labelId);

void X86Emitter_EmitPushEax(X86Emitter *emit);
void X86Emitter_EmitPopEax(X86Emitter *emit);
void X86Emitter_EmitIncEax(X86Emitter *emit);
void X86Emitter_EmitDecEax(X86Emitter *emit);
void X86Emitter_EmitMovEaxImm32(X86Emitter *emit, uint32_t value);
void X86Emitter_EmitAddEaxImm8(X86Emitter *emit, int8_t value);
void X86Emitter_EmitAddEaxImm32(X86Emitter *emit, uint32_t value);
void X86Emitter_EmitCmpEaxImm32(X86Emitter *emit, uint32_t value);
void X86Emitter_EmitIncBytePtrDsEax(X86Emitter *emit);
void X86Emitter_EmitDecBytePtrDsEax(X86Emitter *emit);
void X86Emitter_EmitAddBytePtrDsEaxImm8(X86Emitter *emit, uint8_t value);
void X86Emitter_EmitMovBytePtrDsEaxImm8(X86Emitter *emit, uint8_t value);
void X86Emitter_EmitCmpBytePtrDsEaxImm8(X86Emitter *emit, uint8_t value);
void X86Emitter_EmitMovBytePtrDsEaxCl(X86Emitter *emit);
void X86Emitter_EmitMovzxEcxBytePtrDsEax(X86Emitter *emit);
void X86Emitter_EmitJe(X86Emitter *emit, X86EmitterLabelId labelId);
void X86Emitter_EmitJne(X86Emitter *emit, X86EmitterLabelId labelId);
void X86Emitter_EmitCall(X86Emitter *emit, X86EmitterLabelId labelId);
void X86Emitter_EmitRet(X86Emitter *emit);
void X86Emitter_EmitInt3(X86Emitter *emit);
void X86Emitter_EmitMovEcxEax(X86Emitter *emit);

/// Fixes all jumps/calls/etc. and returns the machine code.
void *X86Emitter_Link(X86Emitter *emit);

#endif // BFOS_X86ASM_H
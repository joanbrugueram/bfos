#include <stddef.h>
#include <stdint.h>
#include "../platform.h"

static inline uint8_t inb(uint16_t port)
{
	unsigned char value;
	__asm__ volatile("inb %1, %0" : "=a" (value) : "Nd" (port));
	return value;
}

/* /--------------------\
 * | CONSOLE MANAGEMENT |
 * \--------------------/ */
#define CONSOLE_COLS 80
#define CONSOLE_ROWS 25

static volatile uint8_t *console = (volatile uint8_t *)0xB8000;
static size_t consolePos = 0;

bool console_output(char ch)
{
	if (ch == '\n')
	{
		// Jump row on newline character
		consolePos += CONSOLE_COLS - (consolePos % CONSOLE_COLS);
	}
	else if (ch >= 0x20 && ch <= 0x7E)
	{
		// ASCII printable character
		console[2*consolePos+0] = ch;
		console[2*consolePos+1] = 0x07;
		consolePos++;
	}
	else
	{
		console[2*consolePos+0] = '?';
		console[2*consolePos+1] = 0xCF;
		consolePos++;
	}

	// Jump line if on end of console
	if (consolePos == CONSOLE_COLS*CONSOLE_ROWS)
	{
		// Move all text one line up
		for (size_t i = 0; i < CONSOLE_COLS*(CONSOLE_ROWS-1); i++)
		{
			console[2*i+0] = console[2*(i+CONSOLE_COLS)+0];
			console[2*i+1] = console[2*(i+CONSOLE_COLS)+1];
		}
		// Erase the last line
		for (size_t i = 0; i < CONSOLE_COLS; i++)
		{
			console[2*(CONSOLE_COLS*(CONSOLE_ROWS-1)+i)+0] = ' ';
			console[2*(CONSOLE_COLS*(CONSOLE_ROWS-1)+i)+1] = 0x07;
		}
		// Go to the beginning of the newly created last line
		consolePos = CONSOLE_COLS*(CONSOLE_ROWS-1);
	}

	return true;
}

/* /---------------------\
 * | KEYBOARD MANAGEMENT |
 * \---------------------/ */
#define PS2_KEYBOARD_DATA_PORT 0x60
#define PS2_KEYBOARD_STATUS_PORT 0x64
#define PS2_KEYBOARD_STATUS_OUTPUT_BUFFER_FULL (1 << 0)

static const char PS2KeyboardScanCodeSet1[0x80] = { // 0 means no scancode or not implemented
	/* 0x00: */     0,    0,  '1',  '2',  '3',  '4',  '5',  '6',
	/* 0x08: */   '7',  '8',  '9',  '0',  '-',  '=',    0,    0,
	/* 0x10: */   'q',  'w',  'e',  'r',  't',  'y',  'u',  'i',
	/* 0x18: */   'o',  'p',  '[',  ']', '\n',    0,  'a',  's',
	/* 0x20: */   'd',  'f',  'g',  'h',  'j',  'k',  'l',  ';',
	/* 0x28: */  '\'',  '`',    0, '\\',  'z',  'x',  'c',  'v',
	/* 0x30: */   'b',  'n',  'm',  ',',  '.',  '/',    0,  '*',
	/* 0x38: */     0,  ' ',    0,    0,    0,    0,    0,    0,
	/* 0x40: */     0,    0,    0,    0,    0,    0,    0,  '7',
	/* 0x48: */   '8',  '9',  '-',  '4',  '5',  '6',  '+',  '1',
	/* 0x50: */   '2',  '3',  '0',  '.',    0,    0,    0,    0,
	/* 0x58: */     0,    0,    0,    0,    0,    0,    0,    0,
	/* 0x60: */     0,    0,    0,    0,    0,    0,    0,    0,
	/* 0x68: */     0,    0,    0,    0,    0,    0,    0,    0,
	/* 0x70: */     0,    0,    0,    0,    0,    0,    0,    0,
	/* 0x78: */     0,    0,    0,    0,    0,    0,    0,    0,
};

static bool isLeftShiftPressed = false;
static bool isRightShiftPressed = false;
static bool isCapsLockOn = false;

bool console_input(char *ch)
{
	while (true)
	{
		uint8_t status = inb(PS2_KEYBOARD_STATUS_PORT);
		if (status & PS2_KEYBOARD_STATUS_OUTPUT_BUFFER_FULL)
		{
			uint8_t scanCode = inb(PS2_KEYBOARD_DATA_PORT);

			uint8_t code = (scanCode & 0x7F);
			bool isPress = (scanCode & 0x80) == 0;

			// Handle shift codes
			if (code == 0x2A)
				isLeftShiftPressed = isPress;
			else if (code == 0x36)
				isRightShiftPressed = isPress;
			else if (code == 0x3A && isPress)
				isCapsLockOn = !isCapsLockOn;

			// Handle normal keys
			if (isPress && PS2KeyboardScanCodeSet1[code] != 0)
			{
				char c = PS2KeyboardScanCodeSet1[code];
				bool caps = (isLeftShiftPressed || isRightShiftPressed || isCapsLockOn);
				if (c >= 'a' && c <= 'z' && caps)
					c += 'A' - 'a';

				console_output(c); // Echo
				*ch = c;
				return true;
			}

		}
	}
}

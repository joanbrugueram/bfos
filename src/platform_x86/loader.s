.global loader                          # making entry point visible to linker

# setting up the Multiboot header - see GRUB docs for details
.set ALIGN,    1<<0                     # align loaded modules on page boundaries
.set MEMINFO,  1<<1                     # provide memory map
.set GFXHEADER,  1<<2                   # specify graphics mode inforation
.set FLAGS, ALIGN | MEMINFO | GFXHEADER # this is the Multiboot 'flag' field
.set MAGIC, 0x1BADB002                  # 'magic number' lets bootloader find the header
.set CHECKSUM, -(MAGIC + FLAGS)         # checksum required

.align 4
	# Common header
	.long MAGIC
	.long FLAGS
	.long CHECKSUM
	# Address header (unused)
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	# Graphics header (used since GFXHEADER bit is set)
	.long 1    # 1 for text mode
	.long 80   # Cols
	.long 25   # Rows
	.long 0    # Depth (ignored)

# reserve initial kernel stack space
stack_bottom:
.skip 16384                             # reserve 16 KiB stack
stack_top:

loader:
	# At this point, %eax contains the multiboot magic number
	#                %ebx contains the multiboot info structure
	cmpl  $0x2BADB002, %eax              # check multiboot magic number
	jne   end

	movl  $stack_top, %esp              # set up the stack, stacks grow downwards

	call  main                          # call C main method

end:
	cli
hang:
	hlt                                 # halt machine should kernel return
	jmp   hang

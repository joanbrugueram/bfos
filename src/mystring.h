#ifndef BFOS_MYSTRING_H
#define BFOS_MYSTRING_H
#ifdef PLATFORM_NATIVE
#include <string.h>
#else

#include <stddef.h>

void *memcpy(void *destination, const void *source, size_t num);
void *memset(void *ptr, int value, size_t num);

#endif
#endif // BFOS_MYSTRING_H
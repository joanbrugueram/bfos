#ifndef BFOS_PLATFORM
#define BFOS_PLATFORM
#ifdef PLATFORM_NATIVE
#else

#if !defined(PLATFORM_X86) && !defined(PLATFORM_ARM)
#error Unsupported platform.
#endif

#include <stdbool.h>

/// Write a character to the console. Returns 0 on error, 1 on success.
bool console_output(char ch);

/// Read a character from the console. Returns 0 on error, 1 on success.
bool console_input(char *ch);

#endif
#endif // BFOS_PLATFORM
#ifndef BFOS_MYSTDIO_H
#define BFOS_MYSTDIO_H
#ifdef PLATFORM_NATIVE
#include <stdio.h>
#else

#define EOF 0x12345678
int putchar(int ch);
int puts(const char *str);

int getchar(void);

#endif
#endif // BFOS_MYSTDIO_H
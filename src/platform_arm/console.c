#include <stddef.h>
#include <stdint.h>
#include "../platform.h"

// See http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dui0159b/I1035564.html
// (ARM Information Center, Integrator/CP User Guide, Overview of UART registers)

#define UART0_ADDRESS 0x16000000
#define UART_DATA_OFFSET 0x000
#define UART_FLAGS_OFFSET 0x018

#define UARTFLAGS_TRANSMIT_FULL (1 << 5)
#define UARTFLAGS_RECEIVE_EMPTY (1 << 4)

bool console_output(char ch)
{
	// Wait until the transmit FIFO is not full
	while (*(volatile uint32_t*)(UART0_ADDRESS + UART_FLAGS_OFFSET) & (UARTFLAGS_TRANSMIT_FULL));
	// Write the character
	*(volatile uint32_t*)(UART0_ADDRESS + UART_DATA_OFFSET) = ch;

	return true;
}

bool console_input(char *ch)
{
	// Wait until the receive FIFO is not empty
	while (*(volatile uint32_t*)(UART0_ADDRESS + UART_FLAGS_OFFSET) & (UARTFLAGS_RECEIVE_EMPTY));
	// Read a character
	uint32_t x = *(volatile uint32_t*)(UART0_ADDRESS + UART_DATA_OFFSET);

	*ch = (char)x;
	if (*ch == '\r')
		*ch = '\n';
	console_output(*ch); // Echo
	return true;
}
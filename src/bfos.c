#include "brainfuck.h"
#include "mystdio.h"
#include <stddef.h>
#include <stdint.h>

void bfos_launch(void)
{
	static const char *bfProgram = 
		#include "../samples/hello.bh"
	;

	puts("Welcome to bfOS!");
	puts("----------------");

	/* Run the program */
	puts("Loading program...");
	BfasmProgram *program = BfasmParseProgram(bfProgram);
	if (program == NULL)
		return;

	puts("Optimizing program...");
	BfasmOptimizeProgram(program);

	#ifdef BFOS_BRAINFUCK_HAVE_COMPILER
	puts("Running program (compiler)...");
	bool success = BfasmCompileProgram(program);
	#else
	puts("Running program (interpreter)...");
	bool success = BfasmRunProgram(program);
	#endif

	puts("");
	if (success)
		puts("Program finished successfully.");
	else
		puts("Program finished because of error.");

	BfasmDestroyProgram(program);
}

int main(void)
{
	/* Start the OS */
	bfos_launch();

	return 0;
}

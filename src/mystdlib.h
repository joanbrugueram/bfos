#ifndef BFOS_MYSTDLIB_H
#define BFOS_MYSTDLIB_H
#ifdef PLATFORM_NATIVE
#include <stdlib.h>
#else

#include <stddef.h>

void abort(void);

void *malloc(size_t size);
void *realloc(void *ptr, size_t size);
void free(void *ptr);

#endif
#endif // BFOS_MYSTDLIB_H
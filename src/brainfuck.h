#ifndef BFOS_BRAINFUCK_H
#define BFOS_BRAINFUCK_H

#include <stdbool.h>

struct BfasmProgram;
typedef struct BfasmProgram BfasmProgram;

/// Destroy an allocated Brainfuck program.
void BfasmDestroyProgram(BfasmProgram *program);

/// Parse a Brainfuck program. Returns a linked list of BfasmInstructions.
BfasmProgram *BfasmParseProgram(const char *programSource);

/// Optimize a Brainfuck program to run faster.
void BfasmOptimizeProgram(BfasmProgram *program);

/// Run a whole Brainfuck program in interpreter mode.
/// Returns true on success, false on error.
bool BfasmRunProgram(BfasmProgram *program);

#if defined(COMPILER_X86)

#define BFOS_BRAINFUCK_HAVE_COMPILER 1

/// Run a whole Brainfuck program in (static) compiler mode.
bool BfasmCompileProgram(BfasmProgram *program);

#endif

#endif // BFOS_BRAINFUCK_H
#include <stddef.h>
#include "mystdio.h"
#include "platform.h"

int putchar(int ch)
{
	return console_output(ch) ? ch : EOF;
}

int puts(const char *str)
{
	while (*str != '\0')
		if (!putchar((char)*str++))
			return EOF;

	return putchar('\n');
}

int getchar(void)
{
	char ch;
	if (!console_input(&ch))
		return EOF;
	return (int)ch;
}
#include <stddef.h>
#include "platform.h"
#include "mystring.h"

void abort(void)
{
	// Hang
	while(1);
}

// TODO implement a real allocator

static unsigned char pool[5000000];
static size_t poolPos = 0;

void *malloc(size_t size)
{
	if (poolPos + size + sizeof(size_t) > sizeof(pool))
		return NULL;

	unsigned char *ptr = &pool[poolPos];
	*(size_t *)ptr = size; // Write memory size for realloc

	poolPos += size + sizeof(size_t);
	return ptr + sizeof(size_t);
}

void *realloc(void *ptr, size_t size)
{
	void *newPtr = malloc(size);
	if (newPtr == NULL)
		return NULL;

	if (ptr != NULL) // Need to copy memory
	{
		size_t oldSize = *((size_t *)ptr - 1);
		memcpy(newPtr, ptr, oldSize);
	}

	return newPtr;
}

void free(void *ptr)
{
	(void)ptr;
}
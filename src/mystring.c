#include <stddef.h>

void *memcpy(void *destination, const void *source, size_t num)
{
	unsigned char *dptr = (unsigned char *)destination;
	const unsigned char *sptr = (const unsigned char *)source;

	for (size_t i = 0; i < num; i++)
		dptr[i] = sptr[i];

	return destination;
}

void *memset(void *ptr, int value, size_t num)
{
	unsigned char *cptr = (unsigned char *)ptr;
	for (size_t i = 0; i < num; i++)
		cptr[i] = (unsigned char)value;

	return ptr;
}
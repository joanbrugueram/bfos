#/bin/bash
set -e

# Compile code
gcc -o bfos.elf src/bfos.c src/brainfuck.c -DPLATFORM_NATIVE -Ofast -Wall -Wextra -std=c99

# Run compiled binary
./bfos.elf
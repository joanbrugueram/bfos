#/bin/bash
set -e

# Compile code
gcc -o bfos.o -c src/bfos.c -DPLATFORM_X86 -DCOMPILER_X86 -Wall -Wextra -nostdlib -nostartfiles -nodefaultlibs -std=c99
gcc -o brainfuck.o -c src/brainfuck.c -DPLATFORM_X86 -DCOMPILER_X86 -Wall -Wextra -nostdlib -nostartfiles -nodefaultlibs -std=c99
gcc -o mystdio.o -c src/mystdio.c -DPLATFORM_X86 -DCOMPILER_X86 -Wall -Wextra -nostdlib -nostartfiles -nodefaultlibs -std=c99
gcc -o mystdlib.o -c src/mystdlib.c -DPLATFORM_X86 -DCOMPILER_X86 -Wall -Wextra -nostdlib -nostartfiles -nodefaultlibs -std=c99
gcc -o mystring.o -c src/mystring.c -DPLATFORM_X86 -DCOMPILER_X86 -Wall -Wextra -nostdlib -nostartfiles -nodefaultlibs -std=c99
gcc -o console.o -c src/platform_x86/console.c -DPLATFORM_X86 -DCOMPILER_X86 -Wall -Wextra -nostdlib -nostartfiles -nodefaultlibs -std=c99
gcc -o x86asm.o -c src/platform_x86/x86asm.c -DPLATFORM_X86 -DCOMPILER_X86 -Wall -Wextra -nostdlib -nostartfiles -nodefaultlibs -std=c99
as -o loader.o src/platform_x86/loader.s
ld -T src/platform_x86/linker.ld -o kernel.bin loader.o bfos.o brainfuck.o mystdio.o mystdlib.o mystring.o console.o x86asm.o

# Copy compiled kernel to ISO folder
cp kernel.bin iso_x86/bfOS.bin

# Make bootable GRUB ISO
grub-mkrescue -o bootable_x86.iso iso_x86

# Launch with QEMU
qemu-system-i386 -vga std -cdrom bootable_x86.iso
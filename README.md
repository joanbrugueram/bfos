bfOS
====
bfOS brings together three obscure ideas in order to create the program. *EVAR*.

- The [Brainfuck](http://en.wikipedia.org/wiki/Brainfuck) programming language.
- Run-time compilation.
- Operating system developement.

Or more precisely, bfOS can be built as a freestanding kernel which, when run, takes a Brainfuck program, compiles it, then executes it.

Features
--------
- Can be built as a platform-independent native program, as an operating system kernel.
- Can run Brainfuck programs either using an interpreter or, on x86 systems, a run-time compiler.
- Can apply some simple optimizations to the Brainfuck program before interpreting or compiling.

Supported platforms
-------------------
- Any platform using the standard C library.
- x86 as a kernel with its own C library.
- ARM (Integrator/CP) as a kernel with its own C library.

Supported execution modes
-------------------------
- Interpreter
- x86 compiler.

TODO list
---------
- Implement x86 kernel keyboard input.
- Implement a real memory allocator for kernel modes.
- Add more platforms and compilers.
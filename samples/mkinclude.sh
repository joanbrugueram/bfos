#/bin/bash
for f in *.bf; do
	echo -n \" > "${f%.bf}.bh"
	tr -cd '><+-.,[]' < "$f" >> "${f%.bf}.bh"
	echo -n \" >> "${f%.bf}.bh"
done
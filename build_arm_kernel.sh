#/bin/bash
set -e

# Compile code
arm-elf-gcc -o bfos.o -c src/bfos.c -DPLATFORM_ARM -Wall -Wextra -nostdlib -nostartfiles -ffreestanding -std=c99
arm-elf-gcc -o brainfuck.o -c src/brainfuck.c -DPLATFORM_ARM -Wall -Wextra -nostdlib -nostartfiles -ffreestanding -std=c99
arm-elf-gcc -o mystdio.o -c src/mystdio.c -DPLATFORM_ARM -Wall -Wextra -nostdlib -nostartfiles -ffreestanding -std=c99
arm-elf-gcc -o mystdlib.o -c src/mystdlib.c -DPLATFORM_ARM -Wall -Wextra -nostdlib -nostartfiles -ffreestanding -std=c99
arm-elf-gcc -o mystring.o -c src/mystring.c -DPLATFORM_ARM -Wall -Wextra -nostdlib -nostartfiles -ffreestanding -std=c99
arm-elf-gcc -o console.o -c src/platform_arm/console.c -DPLATFORM_ARM -Wall -Wextra -nostdlib -nostartfiles -ffreestanding -std=c99
arm-elf-as -o loader.o src/platform_arm/loader.s
arm-elf-ld -T src/platform_arm/linker.ld -o kernel.bin loader.o bfos.o brainfuck.o mystdio.o mystdlib.o mystring.o console.o

qemu-system-arm -m 128 -kernel kernel.bin -serial stdio
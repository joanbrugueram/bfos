#/bin/bash
set -e

# Compile code
gcc -o bfos.elf src/bfos.c src/brainfuck.c src/platform_x86/x86asm.c -DPLATFORM_NATIVE -DCOMPILER_X86 -Ofast -Wall -Wextra -std=c99

# Run compiled code
./bfos.elf